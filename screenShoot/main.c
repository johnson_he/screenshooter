
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define SCREEN_WIDTH   (1024)
#define SCREEN_HEIGHT    (600)
#define SCREEN_DEPTH    (4)

typedef unsigned int UINT;
typedef unsigned short USHORT;
typedef long LONG;
typedef unsigned char UCHAR;

typedef struct tagBITMAPFILEHEADER {
USHORT  bfType;
UINT    bfSize;
USHORT  bfReserved1;
USHORT  bfReserved2;
UINT    bfOffBits;
}__attribute__ ((__packed__))BITMAP_FILE_HEADER;

typedef struct tagBITMAPINFOHEADER{
UINT    biSize;
LONG    biWidth;
LONG    biHeight;
USHORT  biPlanes;
USHORT  biBitCount;
UINT    biCompression;
UINT    biSizeImage;
LONG    biXPelsPerMeter;
LONG    biYPelsPerMeter;
UINT    biClrUsed;
UINT    biClrImportant;
}__attribute__ ((__packed__))BITMAP_INFO_HEADER;

#define RGB565TO1555(rgb) ((unsigned short)((unsigned short)(rgb & 0x001f) | ((unsigned short)(rgb & 0xffc0) >> 1)))


void SaveBMPFile(unsigned char *raw, const char *filename)
{
    BITMAP_FILE_HEADER header;
    BITMAP_INFO_HEADER info;
    UINT *praw = (UINT *)raw;
    UINT *ppixRaw = NULL;
    UINT pixRaw;
    int m_Width = SCREEN_WIDTH, m_Height = SCREEN_HEIGHT;
    int i, j;

    int bmp = open(filename, O_WRONLY | O_CREAT | O_TRUNC);
    if(bmp < 0)
    {
        return;
    }

    header.bfType = 0x4D42;
    header.bfSize = sizeof(header) + sizeof(info) + SCREEN_WIDTH * SCREEN_HEIGHT * SCREEN_DEPTH;
    header.bfReserved1 = 0;
    header.bfReserved2 = 0;
    header.bfOffBits = sizeof(header) + sizeof(info);
    write(bmp, &header, sizeof(header));

    info.biSize = 40;
    info.biWidth = SCREEN_WIDTH;
    info.biHeight = SCREEN_HEIGHT;
    info.biPlanes = 1;
    info.biBitCount = SCREEN_DEPTH * 8;
    info.biCompression = 0;
    info.biSizeImage = SCREEN_WIDTH * SCREEN_HEIGHT * SCREEN_DEPTH;
    info.biXPelsPerMeter = 0;
    info.biYPelsPerMeter = 0;
    info.biClrUsed = 0;
    info.biClrImportant = 0;
    write(bmp, &info, sizeof(info));

    for(i = m_Height - 1; i >= 0; i--)
    {
        ppixRaw = praw + i * m_Width;
        for(j = 0; j < m_Width; j++)
        {
            //pixRaw = RGB565TO1555(*(ppixRaw + j));
            pixRaw = *(ppixRaw + j);
            write(bmp, &pixRaw, SCREEN_DEPTH);
        }
    }
    close(bmp);
}

int main(int argc, char *argv[])
{
    unsigned char buf[SCREEN_WIDTH * SCREEN_HEIGHT * SCREEN_DEPTH];
    const char *filename;
    int fb;

    if(argc == 2)
    {
        filename = argv[1];
    }
    else
    {
        printf("usage: screenShoot xxx.bmp");
        exit(1);
    }

    fb = open("/dev/fb0", O_RDONLY);
    if(fb < 0)
    {
        exit(1);
    }

    printf("reading screen...\n");
    read(fb, buf, SCREEN_HEIGHT * SCREEN_WIDTH * SCREEN_DEPTH);
    close(fb);

    printf("saving screen...\n");
    SaveBMPFile(buf, filename);
    printf("file %s created successfully\n", filename);

    exit(0);
}
